<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Product;

class SearchProduct extends Component
{
    public $search ;
    protected $updatesQueryString = ['search'];
    
    public function mount()
    {
        $this->search = request()->query('search', $this->search);
    }

    public function render()
    {
        
        return view('livewire.search-product',[
            'products'=> Product::where('name','LIKE', '%.$this->search.%')->get(),
                                        
        ]);
    }
}
