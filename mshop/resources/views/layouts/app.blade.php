<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf" content="{{ csrf_token() }}">

  <title>{{ config('app.name') }} | @yield('title')</title>

  @include('layouts.parts.css')
  @livewireStyles
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
   @include('layouts.parts.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('layouts.parts.main_side')
  

  <!-- Content Wrapper. Contains page content -->
   @yield('content')
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  @include('layouts.parts.footer')
  
</div>
<!-- ./wrapper -->

@include('layouts.parts.js')
@yield('js')
@livewireScripts
</body>
</html>
