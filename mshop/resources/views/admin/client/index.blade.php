@extends('layouts.app')

@section('title', 'All Clients')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Categories</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">admin </a></li>
              <li class="breadcrumb-item active">All Clients</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="card card-body">
              <div class="row">
                  <div class="col">
                        <form>
                        <select name="limit" id="">
                            <option value="5" {{ Request::get('limit') == 5? 'selected' : '' }}>5</option>
                            <option value="10" {{ Request::get('limit') == 10? 'selected' : '' }}>10</option>
                            <option value="25" {{ Request::get('limit') == 25? 'selected' : '' }}>25</option>
                        </select>
                        <button type="submit"> Update</button>
                    </form>
                  </div>
                  
                  <div class="col text-center">
                  <form action="">
                        <input type="text" name="search" class="form-control">
                        <button type="submit" class="btn btn-info">Searching</button>
                         <a href="{{route('admin.clients.index')}}">Reset</a>   
                    </form>
                  </div>
                  <div class="col text-right">
                      <a href="{{route('admin.clients.create')}}" class="btn btn-success">create</a>
                  </div>
              </div>
              
          <table class="table table-bordered table-data">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>name</th>
                      <th>email</th>
                      
                  </tr>
              </thead>
              <tbody>
                  @foreach($clients as $client)
                  <tr>
                      <td>{{ $client->id }}</td>
                      <td>{{ $client->name }}</td>
                      <td>{{ $client->email }}</td>
                      
                      <td>
                         <a href="{{route('admin.clients.show', $client)}}" class="btn btn-info">Show</a>
                         <a href="{{route('admin.clients.edit', $client)}}" class="btn btn-primary">Edit</a>
                         <button type="button" class="btn btn-danger delete" data-url="{{ route('admin.clients.destroy', $client) }}">Delete</button>
                            
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
            
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection

@section('js')
    <script>


        $(function(){
              getAllClients();
        })
        
        $(document).on('click', '.delete', function(e){
            //e.preventDefault();
            Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                         
                         //$(`#delete-${$(this).data('id')}`).submit();

                         $.ajax({
                             url: $(this).data('url'),
                             type: 'POST',
                             data: {
                                 _method : 'DELETE',
                                 _token : '{{ csrf_token() }}'
                             },
                             success: function(res){
                                    //console.log('res');
                                    if (result.value) {
                                            Swal.fire(
                                            'Deleted!',
                                            'Your file has been deleted.',
                                            'success'
                                            );
                                            getAllClients();
                                        }
                             }
                         });

                        //$.ajax({
                        //    url: '{{route('admin.categories.show', 1)}}',
                        //    type: 'GET',
                        //    success: function(res){
                        //        console.log(res);
                        //    }
                        //})
                    }
                })

        })

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        getAllClients($(this).text())
    });
    

    function getAllClients(page = 1) {
            $.ajax({
                url: '{{ route('admin.clients.get-clients') }}?page=' + page,
                type: 'GET',
                success: function (res) {
                    $('.table-data tbody').html(res);
                }
            })
        }
    

        
    </script>
    
@endsection